package io.getArrays.userservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "roleID")
    private int id;
    @Column(name = "roleName")
    private String name;
//    @Column(name="defaultPage")
//    private String defaultPage;
@JsonIgnore
    @ManyToMany(mappedBy = "roles")
    @ToString.Exclude
    private List<User> users;



    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", roleName='" + name + '\'' +
               // ", defaultPage='" + defaultPage + '\'' +
                '}';
    }

}
