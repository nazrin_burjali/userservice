package io.getArrays.userservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "`User`")
public class User  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    @Column(name = "userID")
    private int userID;
    @Min(2)
    @Max(100)
    @Pattern(regexp = "^[a-zA-ZıIiİöÖüÜəƏçÇşŞğĞ]+$",  message = "username duzgun deyil")
    @NotBlank(message = "Name can not be blank")
    @Column(name = "name")
    private String name;
    @Min(4)
    @Max(100)
    @Pattern(regexp = "^[a-zA-ZıIiİöÖüÜəƏçÇşŞğĞ]+$",  message = "username duzgun deyil")
    @NotBlank(message = "Username can not be blank")
    @Column(name = "username")
    private String username;
    @NotBlank(message = "Password can not be blank")
    @Min(8)
    @Max(255)
    @Column(name = "password")
    private String password;
    @Email
    @NotBlank(message = "Email can not be blank")
    @Column(name = "email")
    private String email;
    @NotBlank(message = "Phone can not be blank")
    @Min(7)
    @Max(35)
    @Pattern(regexp ="^[0-9]*$", message = "telefon nomresi duzgun deyil")
    @Column(name = "phone")
    private String phone;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "UserRole", joinColumns = @JoinColumn(name = "userID"), inverseJoinColumns = @JoinColumn(name = "roleID"))
    @ToString.Exclude
    private Collection<Role> roles = new ArrayList<>();


    @Override
    public String toString() {
        return "User{" +
                "userID=" + userID +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                '}';
    }
}
