package io.getArrays.userservice.repo;

import io.getArrays.userservice.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo  extends JpaRepository<User, Long> {
    User findByUsername(String username);
    boolean existsByEmail(String email);
}
