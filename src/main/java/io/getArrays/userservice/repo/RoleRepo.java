package io.getArrays.userservice.repo;

import io.getArrays.userservice.domain.Role;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepo extends PagingAndSortingRepository<Role, Long> {

    Role findByName(String name);

}
